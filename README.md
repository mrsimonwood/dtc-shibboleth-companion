# README #

### What is this repository for? ###

* Wordpress Plugin to allow authentication via Shibboleth using only persistent-id (for when this is all IdPs provide), in conjunction with DTC Shibboleth (which is a fork of the Shibboleth plugin). 
* Version 0.1.3

### How do I get set up? ###

* Depends on the DTC Shibboleth plugin.
* Upload to wp-content/plugins via FTP or Plugins > Add New > Upload Plugin
* In the dashboard choose "Activate" or "Activate Plugin"