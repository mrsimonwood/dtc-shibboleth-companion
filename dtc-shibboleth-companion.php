<?php
/*
 Plugin Name: DTC Shibboleth Companion
 Description: Hooks into the DTC Shibboleth plugin to allow authentication with Shibboleth IdPs which may provide nothing more than user's persistent-id 
 Author: Simon Wood
 Version: 0.1.3
 Bitbucket Plugin URI: https://bitbucket.org/mrsimonwood/dtc-shibboleth-companion
 License: Apache 2 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

class WDTC_Login_Buttons
{
	protected $redirect;
	protected $message;
	
	public function __construct($redirect = NULL, $message = NULL)
	{
		$this->redirect = $redirect;
		$this->message = $message;
 	}

	public static function register_styles() {
    	wp_register_style( 'wdtc-buttons-style', plugins_url('css/buttons.css' , __FILE__), array(), '1.0.0', 'all' );
	    wp_register_style( 'tooltips', plugins_url('css/tooltips.css' , __FILE__), array(), '1.0.0', 'all' );
	}

	public function init() {
		wp_enqueue_style('wdtc-buttons-style');
		wp_enqueue_style('tooltips');
	}
	
	public function set_redirect($url)
	{
		if (!filter_var($url, FILTER_VALIDATE_URL) === FALSE)
  			$this->redirect = $url;
	}
	
	public function buttons($alloptions)
	{
   		if (function_exists ('shibboleth_login_form'))
	   	{
	   		$buttons = $this->button("University Login", "<a class=\"tooltip\" \"#\">Sign in via your University's website. <span class=\"classic\"> We are registered with the UK Access Management Federation which means if you can log in to a University account, they can authenticate you on our behalf, without you having to remember another password.</span></a>");
	   		$query_args = array('action'=>false,'reauth'=>'1');
	   	}
	   	if ($alloptions || !$buttons)
	   		$buttons .= $this->button("Wales DTP Login", "<a class=\"tooltip\" \"#\">Sign in with the  username  we sent you.<span class=\"classic\">If you have used this website before, we may have sent you a username and password by email which you should log in with now.</span></a>",$query_args);
   		return '<div id="wdtc-buttons"><div id="wdtc-heading">' . $this->message . '</div>' . $buttons . '</div>';
 	}
	
	protected function button($text, $description, $query_args=NULL)
	{
		$login_url = wp_login_url($this->redirect);
		if ($query_args)
			$login_url = add_query_arg($query_args,$login_url);
		$html = '<div class="wdtc-button-panel"><a href="' . $login_url . '" title="Log in"><span class="wdtc-button" style="width: 100%; text-align: center;">' . $text . '</span></a>';
		if ($description)
			$html .= '<p class="wdtc-button-description">' . $description . '</p>';
		$html .= '</div>';	
		return $html;
	}
	
	public function current_page_url() {
		$pageURL = 'http';
		if ($_SERVER["HTTPS"] == "on")
		$pageURL .= "s";
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		return $pageURL;	
	}
}

function login_form_for_em ($located, $template_name ) {
	if  ( $template_name == 'forms/bookingform/login.php' )
		$located = dirname( __FILE__ ) . '/login.php';
	return $located;
}

// Register styles for WDTC_Login_Buttons
function wdtc_shibboleth_register_styles() {
	WDTC_Login_Buttons::register_styles();
}
add_action( 'wp_enqueue_scripts', 'wdtc_shibboleth_register_styles' );


// This hook is in Events Manager. If that plugin isn't used, our function won't be called
add_filter ('em_locate_template', 'login_form_for_em', 10, 2);


function get_persistent_ids() {
	$persistent_id = $_SERVER['persistent-id'];
	return array_unique(str_getcsv($persistent_id, ';'));
}

function shibboleth_authenticate_user_with_persistent_id($user,$username) 
{
	$users = array();
	foreach (get_persistent_ids() as $persistent_id) {
		$users = array_merge($users, get_users(array('meta_key' => 'persistent_id', 'meta_value' => $persistent_id)));
	}
	if (count($users) == 0) {
		$user = shibboleth_create_new_user_with_persistent_id();
		if (!$user->ID)
			return new WP_Error('missing_data', __('Unable to create account based on data provided.'));
	} elseif (count($users) > 1) {
		return new WP_ERROR('duplicate_ids', __('Multiple accounts exists matching this login.'));
	} else {
		$user = $users[0];
	}
	// Update user data with our metadata
	if (function_exists('shibboleth_update_user_data'))
		shibboleth_update_user_data($user->ID);
	return $user;
}

add_filter('shibboleth_authenticate_user','shibboleth_authenticate_user_with_persistent_id', 10, 2 );


function shibboleth_create_new_user_with_persistent_id() 
{
	$persistent_id=get_persistent_ids()[0];
	if (empty($persistent_id)) return null;
	
	$username = shibboleth_new_random_username();
	$user_data = array('user_login' => $username, 'user_nicename' => $username);
	
	// Add the user to Wordpress
	$user_id = wp_insert_user($user_data);
	$user = new WP_User($user_id);
	
	// Update account and set it as a shibboleth account
	update_user_meta($user->ID, 'persistent_id', $persistent_id);
	
	// User is created, however, is not assigned to any roles.
	if (function_exists('shibboleth_update_user_data'))
		shibboleth_update_user_data($user->ID, true);
	do_action('shibboleth_set_user_roles', $user);
	if ($user->first_name) {
		$displayname = $user->first_name . ' ' . $user->last_name;
	} else {
		$displayname = $username;		
	}
	$user_data = array('ID' => $user_id, 'display_name' => $displayname, 'nickname' => $username);
	wp_update_user($user_data);

	// Send admin notification of new user registration
	shibboleth_new_user_notification($user_id);
	
	return $user;
}


function shibboleth_new_user_notification($user_id) {
	$user = get_userdata( $user_id );

	// The blogname option is escaped with esc_html on the way into the database in sanitize_option
	// we want to reverse this for the plain text arena of emails.
	$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

	$message  = sprintf(__('New user registration (with shibboleth) on your site %s:'), $blogname) . "\r\n\r\n";
	$message .= sprintf(__('Username: %s'), $user->user_login) . "\r\n\r\n";
	$message .= sprintf(__('E-mail: %s'), $user->user_email) . "\r\n";

	@wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), $blogname), $message);

}

/**
 * Generate a new  username that can be both a unique login and a unique nicename.
 */
 
function shibboleth_new_random_username($length = 4, $prefix = 'user', $numbersonly = true) {
    $username = '';
    while ($username == '' || username_exists($username) || get_user_by('slug', $username)) {
   	    $username = shibboleth_random_string($length, $prefix, $numbersonly);
    } 
    return $username;
}

/**
 * Generate a new nicename that is distinct from all existing nicenames.
 */

function shibboleth_new_random_nicename($length = 4, $prefix = 'user', $numbersonly = true) {
    $nicename ='';
    while ($nicename == '' || get_user_by('slug', $nicename)) {
		$nicename = shibboleth_random_string($length, $prefix, $numbersonly);    
    }
    return $nicename;
}

/**
 * Generate a random string
 */
 
function shibboleth_random_string($length, $prefix='', $numbersonly=false) {
    $characters = '0123456789';
	if (!$numbersonly)
		$characters .= 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$randstring = '';
	for ($i = 0; $i < $length; $i++) {
		$randstring .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $prefix . $randstring;	
}

/**
 * Present Persistent-ID in a user's profile to super admins.
 */

function personaloptions_persistent_id($profileuser) { 
	if( is_super_admin() ){ ?>

		<th><label for="persistent_id">Persistent ID</label></th>
			<td>
				<input type="text" name="persistent_id" id="persistent_id" value="<?php echo esc_attr( get_the_author_meta( 'persistent_id', $profileuser->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Persistent ID for Shibboleth (only edit if you know what you are doing!)</span>
			</td>
		</tr>

<?php }
}
add_action ('personal_options', 'personaloptions_persistent_id');

/**
 * Save any changes made to Persistent-ID in a user's profile.
 */
 
function save_additional_user_fields( $user_id ) {
	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;
	if( is_super_admin() ){
		update_usermeta( $user_id, 'persistent_id', $_POST['persistent_id'] );
	}
}
add_action( 'personal_options_update', 'save_additional_user_fields' );
add_action( 'edit_user_profile_update', 'save_additional_user_fields' );

if(is_admin()){
  remove_action("admin_color_scheme_picker", "admin_color_scheme_picker");
}